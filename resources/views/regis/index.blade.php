<a href="/regis/create" class="btn btn-secondary mb-3">Tambah Register</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">email</th>
        <th scope="col">Password</th>
        <th scope="col">Phone</th>
        <th scope="col">Adress</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($regis as $key => $item)
         <tr>
             <td>{{$key + 1}}</td>
             <td>{{$item -> name}}</td> 
             <td>{{$item -> email}}</td>
             <td>{{$item -> password}}</td>
             <td>{{$item -> phone}}</td>
             <td>{{$item -> adress}}</td>
             <td>
                <form action="/regis/{{$item->id}}" method="POST">
                 <a href = "/regis/{{$item->id}}/edit" class ="btn btn-warning btn-sm">Edit</a>
                 @csrf
                 @method('delete')
                 <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
            </td>
         </tr>
      @empty
          <h1> Data Tidak Ada</h1>
      @endforelse
    </tbody>
  </table>