<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Create Data Order</title>

</head>

<body>

<h2>Create Data Order</h2>

<form action="/order" method="POST">
    @csrf
    <div class="form-group">
      <label>Name Jumlah Beli : </label>
      <input type="text" name="jumlah_beli" class="form-control" >
      </div>
      @error('jumlah_beli')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label>Confirmation Number : </label>
      <input type="number" name="confirmation_number" class="form-control" >
    </div>
    @error('confirmation_number')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Product ID : </label>
        <input type="number" name="product_id" class="form-control" >
      </div>
      @error('product_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Shoping Cart ID : </label>
        <input type="number" name="shoping_cart_id" class="form-control" >
      </div>
      @error('shoping_cart_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>
