<a href="/produk/create" class="btn btn-secondary mb-3">Tambah Game</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name Product</th>
        <th scope="col">Price Product</th>
        <th scope="col">Description</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($produk as $key => $item)
         <tr>
             <td>{{$key + 1}}</td>
             <td>{{$item -> nama_barang}}</td> 
             <td>{{$item -> harga_barang}}</td>
             <td>{{$item -> description}}</td>
             <td>
                <form action="/produk/{{$item->id}}" method="POST">
                 <a href = "/produk/{{$item->id}}/edit" class ="btn btn-warning btn-sm">Edit</a>
                 @csrf
                 @method('delete')
                 <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
            </td>
         </tr>
      @empty
          <h1> Data Tidak Ada</h1>
      @endforelse
    </tbody>
  </table>