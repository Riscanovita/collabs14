<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $table = "shopping_cart";
    protected $fillable = [ "count", "total_amount"];
}
