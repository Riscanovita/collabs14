<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Edit Data</title>

</head>

<body>

<h2>Edit Data Register</h2>

<form action="/regis/{{$regis->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Name : </label>
      <input type="text" name="name" value="{{$regis->name}}" class="form-control" >
      </div>
      @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label>Email : </label>
      <input type="email" name="email" value="{{$regis->email}}" class="form-control" >
    </div>
    @error('email')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Password : </label>
        <input type="password" name="password" value="{{$regis->password}}" class="form-control" >
      </div>
      @error('password')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Phone : </label>
        <input type="number" name="phone" value="{{$regis->phone}}" class="form-control" >
      </div>
      @error('phone')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Adress : </label>
      <input type="text" name="adress" value="{{$regis->adress}}" class="form-control" >
    </div>
    @error('adress')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>