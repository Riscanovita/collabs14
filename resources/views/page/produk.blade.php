@extends('layouts.mister')

@section('judul')
halaman produk
@endsection

@section('content')
<h1>produk barang</h1> 
<form action="/welcome" method="post">
@csrf
    <h4>Persale</h4>
    <div class="mb-3">
        <!-- nav1 -->
        <ul class="nav justify-content-end bg-info">
            <li class="nav-item">
              <a class="nav-link bg-dark text-danger " aria-current="page" href="#">Gift Card</a>
            </li>
            <li class="nav-item">
              <a class="nav-link bg-dark text-danger" href="#">TracK Order</a>
            </li>
            <li class="nav-item">
              <a class="nav-link bg-dark text-danger" href="#">Contact Us</a>
            </li> 
          </ul>
          <!-- nav2 -->
          <nav class="navbar navbar-light bg-info justify-content-between" >
            <marquee direction="right"><h1 style="color: rgb(236, 20, 13);font-family: 'Times New Roman', Times, serif; background-color: yellow;text-align:center ;" >welcome to me shop</h1></marquee>
            <form class="form-inline">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
            </form>
          </nav>
        <!-- nav3 -->
        <ul class="nav justify-content-center mt-3 bg-dark">
            <li class="nav-item">
              <a class="nav-link active text-danger mx-3" href="#">HOME</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-danger mx-3" href="#">SHOP</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-danger mx-3" href="#">BLOG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-danger mx-3" href="#">ABOUT US</a>
              </li>
          </ul>
          <div class="container-fluid mt-3 bg-dark">
            <div class="row">
                <!-- kolom1 -->
              <div class="col-3   mb-3">
                <ul class="list-group">
                    <li class="list-group-item">jaket</li>
                    <li class="list-group-item">A second item</li>
                    <li class="list-group-item">A third item</li>
                    <li class="list-group-item">A fourth item</li>
                    <li class="list-group-item">And a fifth one</li>
                    <li class="list-group-item">An item</li>
                    <li class="list-group-item">A second item</li>
                    <li class="list-group-item">A third item</li>
                    <li class="list-group-item">A fourth item</li>
                    <li class="list-group-item">And a fifth one</li>
                    <li class="list-group-item">An item</li>
                    <li class="list-group-item">A second item</li>
                    <li class="list-group-item">A third item</li>
                    <li class="list-group-item">A fourth item</li>
                    <li class="list-group-item">And a fifth one</li>
                  </ul>
                  
              </div>
              <!-- kolom2 -->
              <div class="col-6   mb-3 bg-dark">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <div class="carousel-item active mb-3">
                          <img src="https://th.bing.com/th/id/OIP.gVX_NGP2M2sktaTEH62DaQHaHa?pid=ImgDet&rs=1" width="500px" alt="jaket">
                      </div>
                      <div class="carousel-item mb-3">
                        <img src="https://th.bing.com/th/id/OIP.RbLr1G5SBYvgaCpxxosnuwHaHa?w=212&h=212&c=7&r=0&o=5&pid=1.7" width="500px"  alt="jaket">
                      </div>
                      <div class="carousel-item mb-3" >
                        <img src="https://captainkonveksi.com/wp-content/uploads/2019/08/Bahan-Jaket-1024x810.jpg" width="500px"  alt="jaket">
                      </div>
                      
                    </div>
                    <div class="row mt-1" >
                        <div class="col mb-2">
                            <div class="card" >
                                <img src="https://m.media-amazon.com/images/I/71sOrI1hB9L._AC_SX425_.jpg"  width="200px"  alt="...">
                                <div class="card-body">
                                  <h5 class="card-title">HP Stream 14 inches HD(1366x768)</h5>
                                  <p class="card-text">Accessories may not be original.</p>
                                  <a href="https://www.amazon.com/HP-1366x768-Dual-Core-Processor-Bluetooth/dp/B08F3TNHFJ/ref=sr_1_4?adgrpid=1333708335653699&hvadid=83356975329236&hvbmt=bp&hvdev=c&hvlocphy=3229&hvnetw=o&hvqmt=p&hvtargid=kwd-83357224601257&hydadcr=4399_13165439&keywords=laptop%E2%80%99s&qid=1644997720&sr=8-4&th=1" class="btn btn-primary">Go somewhere</a>
                                </div>
                              </div>
                        </div>
                        <div class="col mb-2">
                            <div class="card">
                                <img src="https://m.media-amazon.com/images/I/71F+S0i2HLL._AC_UY218_.jpg" width="200px"  alt="...">
                                <div class="card-body">
                                  <h5 class="card-title">Newest Dell Inspiron 15 3000 3501 15.6</h5>
                                  <p class="card-text">[Display] 15.6" High-definition display.</p>
                                  <a href="https://www.amazon.com/Dell-Inspiron-i5-1135G7-i7-1065G7-Bluetooth/dp/B09DCDZD4P/ref=sr_1_1_sspa?adgrpid=1333708335653699&hvadid=83356975329236&hvbmt=bp&hvdev=c&hvlocphy=3229&hvnetw=o&hvqmt=p&hvtargid=kwd-83357224601257&hydadcr=4399_13165439&keywords=laptop%E2%80%99s&qid=1644998372&sr=8-1-spons&psc=1&smid=AWC4R9IGPA4LI&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyUlpMR0xWSUxZNTJYJmVuY3J5cHRlZElkPUEwMDUzOTA4UzVUSDlWSkhCSkdZJmVuY3J5cHRlZEFkSWQ9QTA3NjM1MjAzSUJWWUJKUzBEMEQwJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==" class="btn btn-primary">Go somewhere</a>
                                </div>
                              </div>
                        </div>
                        <div class="col mb-3">
                            <div class="card">
                                <img src="https://m.media-amazon.com/images/I/71d5rAq4YaL._AC_UY218_.jpg" width="200px"   alt="...">
                                <div class="card-body">
                                  <h5 class="card-title">Acer Swift X SFX14-41G-R1S6</h5>
                                  <p class="card-text">Series	SFX14-41G-R1S6 Ultimate Performance.
                                    </p>
                                  <a href="https://www.amazon.com/Acer-SFX14-41G-R1S6-Creator-LPDDR4X-Backlit/dp/B093TK1PXF/ref=sr_1_1_sspa?adgrpid=1333708335653699&hvadid=83356975329236&hvbmt=bp&hvdev=c&hvlocphy=3229&hvnetw=o&hvqmt=p&hvtargid=kwd-83357224601257&hydadcr=4399_13165439&keywords=laptop%E2%80%99s&qid=1644997720&sr=8-1-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzMjNDOUYwNkdPVkFXJmVuY3J5cHRlZElkPUEwNDI0ODEwMkZSVFE4UVFJTzVLTiZlbmNyeXB0ZWRBZElkPUExMDE3MzkxUUsxU0hGQ1BYMFE2JndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ&th=1" class="btn btn-primary">Go somewhere</a>
                                </div>
                                
                              </div>
                        </div>
                        
                       
                    </div>
                    
                  </div>
                  
              </div>

              <!-- kolom3 -->
              <div class="col-3   mb-3 bg-dark" >
                <li class="list-group-item">Feature 1</li>
                <li class="list-group-item">Feature 2</li>
                <li class="list-group-item">Feature 3</li>
                <li class="list-group-item">Feature 4</li>
                <li class="list-group-item">Feature 5</li>
                <li class="list-group-item">Feature 6</li>
                <form>
                  <div class="form-group mt-5 text-danger" >
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                  </div>
                  <div class="form-group text-danger">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1">
                  </div>
                  <div class="form-group form-check text-info">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner mt-5">
                    <div class="carousel-item active">
                      <img src="https://www.3lyonprestige.com/storage/32000/80/a57b3a035019213e009655a494d2b035.png" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                      <img src="https://1.bp.blogspot.com/-ljRaHylZiCA/XPTCmmMmIqI/AAAAAAAAZIA/nq7DtdjaDv021sc3NMW7lZWNEjvAyIrPgCLcBGAs/w1200-h630-p-k-no-nu/COD%2B1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                      <img src="https://1.bp.blogspot.com/-ScRtd4_AH_g/XW3ToZFpETI/AAAAAAAAEv4/wNdzc9SGyzM6MssXVZLWz7DmsNZZfJ4wgCLcBGAs/s1600/cara-menghapus-akun-lazada.png" class="d-block w-100" alt="...">
                    </div>
                  </div>
                 <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </button>
                  <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </button>
                </div>
                </div>
                
            
            </div>
            <footer style="background-color: black; padding: 5px; ">
              <h5 style="color: aliceblue; text-align:center">copyright &copy; 2019 by pks digital school</h5>
            </footer>
          </div>
          
    </div>

@endsection