<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rolle extends Model
{
    protected $table = "rolle";
    protected $fillable = [ "name"];
}
