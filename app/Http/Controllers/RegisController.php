<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Regis;
class RegisController extends Controller
{
    public function create(){
        return view('regis.create');
    }

    public function store(Request $request){
        
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required', 
            'phone' => 'required', 
            'adress' => 'required', 
        ]);
        $regis = new Regis;
 
        $regis->name = $request->name;
        $regis->email = $request->email;
        $regis->password = $request->password;
        $regis->phone = $request->phone;
        $regis->adress = $request->adress;
        $regis->save();
        return redirect('/regis');
    }

    public function index(){
        $regis=Regis::all();
        return view('regis.index',compact('regis'));
    }

    public function show($regis_id){
        $regis=Regis::where('id', $regis_id);
        return view('regis.show', compact('regis'));
    }

    public function edit($regis_id){
        $regis=Regis::where('id', $regis_id)->first();
        return view('regis.edit', compact('regis'));
        }
    
        public function update(Request $request, $regis_id){
            $request->validate([
                'name' => 'required',
                'email' => 'required',
                'password' => 'required', 
                'phone' => 'required', 
                'adress' => 'required', 
            ]);
    
            $regis=Regis::find($regis_id);
            $regis->name=$request['name'];
            $regis->email=$request['email'];
            $regis->password=$request['password'];
            $regis->phone=$request['phone'];
            $regis->adress=$request['adress'];
            $regis->save();
            return redirect('/regis');
        }
        public function destroy($regis_id){
            $regis = Regis::find($regis_id);
            $regis->delete();
            return redirect('/regis');
        }
}
