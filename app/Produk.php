<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "produk";
    protected $fillable = [ "nama_barang", "harga_barang", "description", "category_id"];
}
