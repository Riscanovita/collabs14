<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegisterUser;
use Illuminate\Support\Vacades\Hash;
use Illuminate\Support\Vacades\Validator;

class userController extends Controller{
use RegisterUser;

protected $redirectTo = RouteServiceProvider::HOME;
public function __construct()
    {
        $this->middleware('guest');
    }

protected function validator(array $data){
    return validator :: make($data, [
        'name' => ['required'],
        'email' => ['required'],
        'pwd' => ['required'],
        'phone' => ['require'],
        'adress' => ['require']
    ]);
}

protected function create(array $data)
{
    return User ::create([
    'name' => $data['name'],
    'email' => $data['email'],
    'pwd' => $data['pwd'],
    'phone' => $data['phone'],
    'adress' => $data['adress'],
]);
}
}