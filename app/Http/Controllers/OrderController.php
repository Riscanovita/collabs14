<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
class OrderController extends Controller
{
    public function create(){
        return view('order.create');
    }

    public function store(Request $request){
        
        $request->validate([
            'jumlah_beli' => 'required',
            'confirmation_number' => 'required',
            'product_id' => 'required', 
            'shoping_cart_id' => 'required', 
        ]);
        $order = new Order;
 
        $order->jumlah_beli = $request->jumlah_beli;
        $order->confirmation_number = $request->confirmation_number;
        $order->product_id = $request->product_id;
        $order->shoping_cart_id = $request->shoping_cart_id;
        $order->save();
        return redirect('/order');
    }
    public function index(){
        $order=Order::all();
        return view('order.index',compact('order'));
    }

    public function edit($order_id){
        $order=Order::where('id', $order_id)->first();
        return view('order.edit', compact('order'));
    }
    
    public function update(Request $request, $order_id){
        $request->validate([
            'jumlah_beli' => 'required',
            'confirmation_number' => 'required',
            'product_id' => 'required', 
            'shoping_cart_id' => 'required', 
        ]);

        $order=Order::find($order_id);
        $order->jumlah_beli=$request['jumlah_beli'];
        $order->confirmation_number=$request['confirmation_number'];
        $order->product_id=$request['product_id'];
        
        $order->shoping_cart_id=$request['shoping_cart_id'];
        $order->save();
        return redirect('/order');
    }

        public function destroy($order_id){
            $order = Order::find($order_id);
            $order->delete();
            return redirect('/order');
        }
}
