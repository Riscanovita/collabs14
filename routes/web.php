<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');
Route::get('/registrasi','AuthController@register');  // ini masuknya ke registrasi.blade bukan bawaan laravel
Route::post('/welcome','AuthController@kirim');
Route::get('/Order','AuthController@Order');
Route::get('/produk','AuthController@produk');
Route::get('/master', function(){
    return view('page.master');
});
Route::get('/login', function(){
    return view('regis.login');
});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');



//regis
Route::get('/regis/create','RegisController@create');
Route::post('/regis','RegisController@store');
Route::get('/regis','RegisController@index');
Route::get('/regis/{regis_id}','RegisController@show');
Route::get('/regis/{regis_id}/edit','RegisController@edit');
Route::put('/regis/{regis_id}', 'RegisController@update');
Route::delete('/regis/{regis_id}','RegisController@destroy' );

//produk
Route::get('/produk/create','ProdukController@create');
Route::post('/produk','ProdukController@store');
Route::get('/produk','ProdukController@index');
Route::get('/produk/{produk_id}/edit','ProdukController@edit');
Route::put('/produk/{produk_id}', 'ProdukController@update');
Route::delete('/produk/{produk_id}','ProdukController@destroy' );
route::get('/login', function () {
    return view('regis.login');
});
Route::group(['middleware' => ['auth']], function(){
//order
Route::get('/order/create','OrderController@create');
Route::post('/order','OrderController@store');
Route::get('/order','OrderController@index');
Route::get('/order/{order_id}/edit','OrderController@edit');
Route::put('/order/{order_id}', 'OrderController@update');
Route::delete('/order/{order_id}','OrderController@destroy' );
});
