<?php

namespace App\Http\Controllers;
use App\Produk;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    public function create(){
        return view('produk.create');
    }

    public function store(Request $request){
        
        $request->validate([
            'nama_barang' => 'required',
            'harga_barang' => 'required',
            'description' => 'required', 
        ]);
        $produk = new Produk;
 
        $produk->nama_barang = $request->nama_barang;
        $produk->harga_barang = $request->harga_barang;
        $produk->description = $request->description;
        $produk->save();
        return redirect('/produk');
    }
    public function index(){
        $produk=Produk::all();
        return view('produk.index',compact('produk'));
    }

    public function edit($produk_id){
        $produk=Produk::where('id', $produk_id)->first();
        return view('produk.edit', compact('produk'));
        }
    
        public function update(Request $request, $produk_id){
            $request->validate([
                'nama_barang' => 'required',
                'harga_barang' => 'required',
                'description' => 'required', 
            ]);
    
            $produk=Produk::find($produk_id);
            $produk->nama_barang=$request['nama_barang'];
            $produk->harga_barang=$request['harga_barang'];
            $produk->description=$request['description'];
            $produk->save();
            return redirect('/produk');
        }

        public function destroy($produk_id){
            $produk = Produk::find($produk_id);
            $produk->delete();
            return redirect('/produk');
        }
}
