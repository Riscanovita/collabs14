<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "Order";
    protected $fillable = [ "name", "nama_barang", "confirmation_number", "product_id", "shopping_id"];
}
