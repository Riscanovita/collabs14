<a href="/order/create" class="btn btn-secondary mb-3">Tambah Game</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Jumlah Beli</th>
        <th scope="col">Confirmation Number</th>
        <th scope="col">Product ID</th>
        <th scope="col">Shoping Cart ID</th>
      </tr>
      </tr>
    </thead>
    <tbody>
      @forelse ($order as $key => $item)
         <tr>
             <td>{{$key + 1}}</td>
             <td>{{$item -> jumlah_beli}}</td> 
             <td>{{$item -> confirmation_number}}</td>
             <td>{{$item -> product_id}}</td>
             <td>{{$item -> shoping_cart_id}}</td>
             <td>
                <form action="/order/{{$item->id}}" method="POST">
                 <a href = "/order/{{$item->id}}/edit" class ="btn btn-warning btn-sm">Edit</a>
                 @csrf
                 @method('delete')
                 <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
            </td>
         </tr>
      @empty
          <h1> Data Tidak Ada</h1>
      @endforelse
    </tbody>
  </table>